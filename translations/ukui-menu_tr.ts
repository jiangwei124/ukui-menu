<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr_TR">
<context>
    <name>FullMainWindow</name>
    <message>
        <location filename="../src/UserInterface/full_mainwindow.cpp" line="87"/>
        <source>Search</source>
        <translation type="unfinished">Ara</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/full_mainwindow.cpp" line="145"/>
        <source>All</source>
        <translation type="unfinished">Tümü</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/full_mainwindow.cpp" line="147"/>
        <source>Letter</source>
        <translation type="unfinished">Alfabetik</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/full_mainwindow.cpp" line="149"/>
        <source>Function</source>
        <translation type="unfinished">Kategori</translation>
    </message>
</context>
<context>
    <name>FunctionClassifyButton</name>
    <message>
        <source>Mobile</source>
        <translation type="vanished">Mobil</translation>
    </message>
    <message>
        <source>Internet</source>
        <translation type="vanished">İnternet</translation>
    </message>
    <message>
        <source>Social</source>
        <translation type="vanished">Sosyal</translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="vanished">Video</translation>
    </message>
    <message>
        <source>Development</source>
        <translation type="vanished">Programlama</translation>
    </message>
    <message>
        <source>Image</source>
        <translation type="vanished">Grafik</translation>
    </message>
    <message>
        <source>Game</source>
        <translation type="vanished">Oyunlar</translation>
    </message>
    <message>
        <source>Office</source>
        <translation type="vanished">Ofis Uygulamaları</translation>
    </message>
    <message>
        <source>Education</source>
        <translation type="vanished">Eğitim</translation>
    </message>
    <message>
        <source>System</source>
        <translation type="vanished">Sistem</translation>
    </message>
    <message>
        <source>Others</source>
        <translation type="vanished">Diğer</translation>
    </message>
</context>
<context>
    <name>FunctionWidget</name>
    <message>
        <source>Mobile</source>
        <translation type="vanished">Mobil</translation>
    </message>
    <message>
        <source>Internet</source>
        <translation type="vanished">İnternet</translation>
    </message>
    <message>
        <source>Social</source>
        <translation type="vanished">Sosyal</translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="vanished">Video</translation>
    </message>
    <message>
        <source>Development</source>
        <translation type="vanished">Programlama</translation>
    </message>
    <message>
        <source>Image</source>
        <translation type="vanished">Grafik</translation>
    </message>
    <message>
        <source>Game</source>
        <translation type="vanished">Oyunlar</translation>
    </message>
    <message>
        <source>Office</source>
        <translation type="vanished">Ofis Uygulamaları</translation>
    </message>
    <message>
        <source>Education</source>
        <translation type="vanished">Eğitim</translation>
    </message>
    <message>
        <source>System</source>
        <translation type="vanished">Sistem</translation>
    </message>
    <message>
        <source>Others</source>
        <translation type="vanished">Diğer</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/Widget/function_Widget.cpp" line="199"/>
        <source>Search</source>
        <translation type="unfinished">Ara</translation>
    </message>
</context>
<context>
    <name>GetModelData</name>
    <message>
        <source>Mobile</source>
        <translation type="obsolete">Mobil</translation>
    </message>
    <message>
        <source>Internet</source>
        <translation type="obsolete">İnternet</translation>
    </message>
    <message>
        <source>Social</source>
        <translation type="obsolete">Sosyal</translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="obsolete">Video</translation>
    </message>
    <message>
        <source>Development</source>
        <translation type="obsolete">Programlama</translation>
    </message>
    <message>
        <source>Image</source>
        <translation type="obsolete">Grafik</translation>
    </message>
    <message>
        <source>Game</source>
        <translation type="obsolete">Oyunlar</translation>
    </message>
    <message>
        <source>Office</source>
        <translation type="obsolete">Ofis Uygulamaları</translation>
    </message>
    <message>
        <source>Education</source>
        <translation type="obsolete">Eğitim</translation>
    </message>
    <message>
        <source>System</source>
        <translation type="obsolete">Sistem</translation>
    </message>
    <message>
        <source>Others</source>
        <translation type="obsolete">Diğer</translation>
    </message>
</context>
<context>
    <name>ItemDelegate</name>
    <message>
        <location filename="../src/UserInterface/ViewItem/itemdelegate.cpp" line="176"/>
        <source>Open the function sort menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/UserInterface/ViewItem/itemdelegate.cpp" line="178"/>
        <source>Open the alphabetical menu</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainViewWidget</name>
    <message>
        <source>All</source>
        <translation type="obsolete">Tümü</translation>
    </message>
    <message>
        <source>Letter</source>
        <translation type="obsolete">Alfabetik</translation>
    </message>
    <message>
        <source>Function</source>
        <translation type="obsolete">Kategori</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">Ara</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="497"/>
        <source>collection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="313"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="490"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="551"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="1119"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="1120"/>
        <source>All</source>
        <translation type="unfinished">Tümü</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="309"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="331"/>
        <source>Search</source>
        <translation type="unfinished">Ara</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="449"/>
        <source>No recent files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="498"/>
        <source>recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="499"/>
        <source>Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="501"/>
        <source>PowerOff</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="553"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="1132"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="1133"/>
        <source>Letter</source>
        <translation type="unfinished">Alfabetik</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/mainwindow.cpp" line="555"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="1145"/>
        <location filename="../src/UserInterface/mainwindow.cpp" line="1146"/>
        <source>Function</source>
        <translation type="unfinished">Kategori</translation>
    </message>
</context>
<context>
    <name>PushButton</name>
    <message>
        <source>Mobile</source>
        <translation type="vanished">Mobil</translation>
    </message>
    <message>
        <source>Internet</source>
        <translation type="vanished">İnternet</translation>
    </message>
    <message>
        <source>Social</source>
        <translation type="vanished">Sosyal</translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="vanished">Video</translation>
    </message>
    <message>
        <source>Development</source>
        <translation type="vanished">Programlama</translation>
    </message>
    <message>
        <source>Image</source>
        <translation type="vanished">Grafik</translation>
    </message>
    <message>
        <source>Game</source>
        <translation type="vanished">Oyunlar</translation>
    </message>
    <message>
        <source>Office</source>
        <translation type="vanished">Ofis Uygulamaları</translation>
    </message>
    <message>
        <source>Education</source>
        <translation type="vanished">Eğitim</translation>
    </message>
    <message>
        <source>System</source>
        <translation type="vanished">Sistem</translation>
    </message>
    <message>
        <source>Others</source>
        <translation type="vanished">Diğer</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="73"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="29"/>
        <source>Office</source>
        <translation type="unfinished">Ofis Uygulamaları</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="74"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="30"/>
        <source>Development</source>
        <translation type="unfinished">Programlama</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="75"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="31"/>
        <source>Image</source>
        <translation type="unfinished">Grafik</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="76"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="32"/>
        <source>Video</source>
        <translation type="unfinished">Video</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="77"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="33"/>
        <source>Internet</source>
        <translation type="unfinished">İnternet</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="78"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="34"/>
        <source>Game</source>
        <translation type="unfinished">Oyunlar</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="79"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="35"/>
        <source>Education</source>
        <translation type="unfinished">Eğitim</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="80"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="36"/>
        <source>Social</source>
        <translation type="unfinished">Sosyal</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="81"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="37"/>
        <source>System</source>
        <translation type="unfinished">Sistem</translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="82"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="38"/>
        <source>Safe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/BackProcess/Interface/ukuimenuinterface.cpp" line="83"/>
        <location filename="../src/UserInterface/Widget/function_button_widget.cpp" line="39"/>
        <source>Others</source>
        <translation type="unfinished">Diğer</translation>
    </message>
</context>
<context>
    <name>RightClickMenu</name>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="213"/>
        <source>Pin to all</source>
        <translation>Tümünü sabitle</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="216"/>
        <source>Unpin from all</source>
        <translation>Tüm sabitlemeleri kaldır</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="226"/>
        <source>Pin to taskbar</source>
        <translation>Görev çubuğuna sabitle</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="229"/>
        <source>Unpin from taskbar</source>
        <translation>Görev çubuğundan sabitlemeyi kaldır</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="232"/>
        <source>Add to desktop shortcuts</source>
        <translation>Masaüstü kısayollarına ekle</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="236"/>
        <source>Pin to collection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="239"/>
        <source>Remove from collection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="254"/>
        <source>Uninstall</source>
        <translation>Kaldır</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="274"/>
        <source>Switch user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="281"/>
        <source>Hibernate</source>
        <translation>Beklemeye Al</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="288"/>
        <source>Sleep</source>
        <translation>Uyku Modu</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="292"/>
        <source>Lock Screen</source>
        <translation>Ekranı Kilitle</translation>
    </message>
    <message>
        <source>Switch User</source>
        <translation type="vanished">Kullanıcı Değiştir</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="297"/>
        <source>Log Out</source>
        <translation>Çıkış</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="304"/>
        <source>Restart</source>
        <translation>Yeniden Başlat</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="311"/>
        <source>Power Off</source>
        <translation>Kapat</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/rightclickmenu.cpp" line="342"/>
        <source>Personalize this list</source>
        <translation>Bu listeyi özelleştirin</translation>
    </message>
</context>
<context>
    <name>SideBarWidget</name>
    <message>
        <source>All</source>
        <translation type="vanished">Tümü</translation>
    </message>
    <message>
        <source>Letter</source>
        <translation type="vanished">Alfabetik</translation>
    </message>
    <message>
        <source>Function</source>
        <translation type="vanished">Kategori</translation>
    </message>
    <message>
        <source>Trash</source>
        <translation type="obsolete">Çöp</translation>
    </message>
    <message>
        <source>Computer</source>
        <translation type="vanished">Bilgisayar</translation>
    </message>
    <message>
        <source>Personal</source>
        <translation type="vanished">Kişisel</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Ayarlar</translation>
    </message>
    <message>
        <source>Recycle Bin</source>
        <translation type="vanished">Çöp</translation>
    </message>
    <message>
        <source>Power</source>
        <translation type="vanished">Kapat</translation>
    </message>
</context>
<context>
    <name>SplitBarFrame</name>
    <message>
        <source>Mobile</source>
        <translation type="obsolete">Mobil</translation>
    </message>
    <message>
        <source>Internet</source>
        <translation type="obsolete">İnternet</translation>
    </message>
    <message>
        <source>Social</source>
        <translation type="obsolete">Sosyal</translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="obsolete">Video</translation>
    </message>
    <message>
        <source>Development</source>
        <translation type="obsolete">Programlama</translation>
    </message>
    <message>
        <source>Image</source>
        <translation type="obsolete">Grafik</translation>
    </message>
    <message>
        <source>Game</source>
        <translation type="obsolete">Oyunlar</translation>
    </message>
    <message>
        <source>Office</source>
        <translation type="obsolete">Ofis Uygulamaları</translation>
    </message>
    <message>
        <source>Education</source>
        <translation type="obsolete">Eğitim</translation>
    </message>
    <message>
        <source>System</source>
        <translation type="obsolete">Sistem</translation>
    </message>
    <message>
        <source>Others</source>
        <translation type="obsolete">Diğer</translation>
    </message>
</context>
<context>
    <name>TabletRightClickMenu</name>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/tabletrightclickmenu.cpp" line="220"/>
        <source>Pin to taskbar</source>
        <translation type="unfinished">Görev çubuğuna sabitle</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/tabletrightclickmenu.cpp" line="223"/>
        <source>Unpin from taskbar</source>
        <translation type="unfinished">Görev çubuğundan sabitlemeyi kaldır</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/tabletrightclickmenu.cpp" line="229"/>
        <source>Add to desktop shortcuts</source>
        <translation type="unfinished">Masaüstü kısayollarına ekle</translation>
    </message>
    <message>
        <location filename="../src/UserInterface/RightClickMenu/tabletrightclickmenu.cpp" line="248"/>
        <source>Uninstall</source>
        <translation type="unfinished">Kaldır</translation>
    </message>
</context>
<context>
    <name>TimeWidget</name>
    <message>
        <source>Search</source>
        <translation type="obsolete">Ara</translation>
    </message>
</context>
</TS>

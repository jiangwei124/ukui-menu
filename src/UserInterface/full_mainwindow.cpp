#include "full_mainwindow.h"
#include <KWindowEffects>
#include <QAction>
#include <QTranslator>
#include "utility.h"

FullMainWindow::FullMainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    this->setWindowFlags(Qt::CustomizeWindowHint | Qt::FramelessWindowHint | Qt::X11BypassWindowManagerHint);
    this->setAttribute(Qt::WA_TranslucentBackground, true);
    this->setAutoFillBackground(false);
    Style::initWidStyle();
    this->setFixedSize(Style::m_availableScreenWidth, Style::m_availableScreenHeight);
    initLayout();
    initSearchUI();
    initButtonUI();
    initAppListUI();
    initMenu();
    initTabOrder();
    changeStyle();
    initConnect();
}

FullMainWindow::~FullMainWindow()
{
}

void FullMainWindow::initLayout()
{
    centralwidget = new QWidget(this);
    verticalLayout = new QVBoxLayout(centralwidget);
    verticalLayout->setSpacing(0);
    verticalLayout->setContentsMargins(0, 48, 0, 0);
    topHorizontalLayout = new QHBoxLayout();
    topHorizontalLayout->setSpacing(8);
    topHorizontalLayout->setContentsMargins(155, 0, 35, 0);
    horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    bottomHorizonLayout = new QHBoxLayout();
    bottomHorizonLayout->setSpacing(0);
    bottomHorizonLayout->setContentsMargins(0, 0, 0, 0);
}

void FullMainWindow::initButtonUI()
{
    m_buttonStyle = QString("%1{border-radius:24px; background: rgba(255, 255, 255, 0.1);}"
                            "%1:hover {border-radius:24px; background: rgba(255, 255, 255, 0.2);}"
                            "%1:pressed {border-radius:24px; background: rgba(255, 255, 255, 0.3);}");
    m_lineEdit->setFocusPolicy(Qt::StrongFocus);
    horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    m_fullSelectToolButton = new QPushButton(centralwidget);
//    fullSelectToolButton->setStyleSheet(m_buttonStyle.arg("QToolButton"));
    m_fullSelectToolButton->setObjectName(QString::fromUtf8("fullSelectToolButton"));
    m_fullSelectToolButton->setMinimumSize(QSize(48, 48));
    m_fullSelectToolButton->installEventFilter(this);
    m_fullSelectToolButton->setFocus();
    QIcon selectIcon;
    selectIcon.addFile(QString::fromUtf8(":/data/img/mainviewwidget/full-function.svg"), QSize(), QIcon::Normal, QIcon::Off);
    m_fullSelectToolButton->setIcon(selectIcon);

    m_fullSelectMenuButton = new RotationLabel(this);
    m_fullSelectMenuButton->installEventFilter(this);
    m_fullSelectMenuButton->setStyleSheet("background: transparent;");
    m_fullSelectMenuButton->setFixedSize(QSize(16, 34));
    m_fullSelectMenuButton->setAcceptDrops(true);
    m_fullSelectMenuButton->setFocusPolicy(Qt::StrongFocus);
    m_fullSelectMenuButton->setIcon(QPixmap(":/data/img/mainviewwidget/DM-arrow-2x.png"));
    QPalette palete;
    palete.setColor(QPalette::NoRole, Qt::white);
    m_fullSelectMenuButton->setPalette(palete);
    m_minPushButton = new QPushButton(centralwidget);
    m_minPushButton->setObjectName(QString::fromUtf8("minPushButton"));
    m_minPushButton->setFixedSize(QSize(48, 48));
    m_minPushButton->setFlat(true);
    m_minPushButton->installEventFilter(this);
}

void FullMainWindow::initSearchUI()
{
    m_searchAppThread = new SearchAppThread;
    m_lineEdit = new QLineEdit(centralwidget);
    m_lineEdit->setFixedSize(QSize(372, 36));
    m_lineEdit->setLayoutDirection(Qt::LeftToRight);
    m_lineEdit->installEventFilter(this);
    m_lineEdit->setFrame(false);
    m_queryWid = new QWidget;
    m_queryWid->setParent(m_lineEdit);
    m_queryWid->setFocusPolicy(Qt::NoFocus);
    m_queryWid->setFixedSize(372, 36);
    QHBoxLayout *queryWidLayout = new QHBoxLayout;
    queryWidLayout->setContentsMargins(0, 0, 0, 0);
    queryWidLayout->setSpacing(5);
    m_queryWid->setLayout(queryWidLayout);
    char style[200];
    QPixmap pixmap = loadSvg(QString(":/data/img/mainviewwidget/full-search.svg"), 16);

    if (QGSettings::isSchemaInstalled(QString("org.ukui.style").toLocal8Bit())) {
        QGSettings *gsetting = new QGSettings("org.ukui.style", QByteArray(), this);

        if (gsetting->keys().contains("styleName")) {
            if (/*gsetting->get("styleName").toString() == "ukui-light"*/false) {
                pixmap = drawSymbolicBlackColoredPixmap(pixmap);
                sprintf(style, "QLineEdit{border:1px solid %s;background-color:%s;border-radius:17px;color:#000000;}",
                        QueryLineEditClickedBorderDefault, QueryLineEditDefaultBackground);
            } else {
                pixmap = drawSymbolicColoredPixmap(pixmap); //反白
                sprintf(style, "QLineEdit{border:1px solid %s;background-color:%s;border-radius:17px;color:#ffffff;}",
                        QueryLineEditClickedBorderDefault, QueryLineEditBackground);
            }
        }

        connect(gsetting, &QGSettings::changed, [ = ](QString key) {
            if ("systemFont" == key || "systemFontSize" == key) {
                m_queryWid->layout()->removeWidget(m_queryText);
                m_queryText->setParent(nullptr);
                resetEditline();
                m_fullCommonPage->repaintWidget();
                m_fullFunctionPage->repaintWidget();
                m_fullLetterPage->repaintWidget();
            }

            if (key.contains(QString("styleName"))) {
                changeStyle();
            }
        });
    }

    m_lineEdit->setStyleSheet(style);
    pixmap.setDevicePixelRatio(qApp->devicePixelRatio());
    m_queryIcon = new QLabel;
    m_queryIcon->setFixedSize(pixmap.size());
    m_queryIcon->setPixmap(pixmap);
    m_queryText = new QLabel;
    QPalette pa;
    pa.setColor(QPalette::Text, Qt::white);
    m_queryText->setPalette(pa);
    m_queryText->setText(tr("Search"));
    m_queryText->adjustSize();
    queryWidLayout->addWidget(m_queryIcon);
    queryWidLayout->addWidget(m_queryText);
    queryWidLayout->setAlignment(Qt::AlignCenter);
}

void FullMainWindow::initAppListUI()
{
    m_fullStackedWidget = new QStackedWidget(centralwidget);
    m_fullCommonPage = new FullCommonUseWidget(m_fullStackedWidget);
    m_fullStackedWidget->addWidget(m_fullCommonPage);
    m_fullLetterPage = new FullLetterWidget(m_fullStackedWidget);
    m_fullStackedWidget->addWidget(m_fullLetterPage);
    m_fullFunctionPage = new FullFunctionWidget(m_fullStackedWidget);
    m_fullStackedWidget->addWidget(m_fullFunctionPage);
    m_fullResultPage = new FullSearchResultWidget(m_fullStackedWidget);
    m_fullStackedWidget->addWidget(m_fullResultPage);
    bottomHorizonLayout->addWidget(m_fullStackedWidget);
    //    bottomHorizonLayout->addWidget(verticalScrollBar);
    topHorizontalLayout->addItem(horizontalSpacer);
    topHorizontalLayout->addWidget(m_lineEdit);
    topHorizontalLayout->addItem(horizontalSpacer_2);
    topHorizontalLayout->addWidget(m_fullSelectToolButton);
    topHorizontalLayout->addWidget(m_fullSelectMenuButton);
    topHorizontalLayout->addWidget(m_minPushButton);
    verticalLayout->addLayout(topHorizontalLayout);
    verticalLayout->addLayout(bottomHorizonLayout);
    this->setCentralWidget(centralwidget);
    m_fullStackedWidget->setCurrentIndex(0);
}

void FullMainWindow::initMenu()
{
    m_dropDownMenu = new MenuBox(this);
    m_dropDownMenu->setFixedWidth(Style::DropMenuWidth);
    m_allAction = new QAction(m_dropDownMenu);
    m_letterAction = new QAction(m_dropDownMenu);
    m_funcAction = new QAction(m_dropDownMenu);
    m_allAction->setText(tr("All"));
    m_allAction->setCheckable(true);
    m_letterAction->setText(tr("Letter"));
    m_letterAction->setCheckable(true);
    m_funcAction->setText(tr("Function"));
    m_funcAction->setCheckable(true);
    m_dropDownMenu->addAction(m_allAction);
    m_dropDownMenu->addAction(m_letterAction);
    m_dropDownMenu->addAction(m_funcAction);
    m_allAction->setChecked(true);
}

void FullMainWindow::initTabOrder()
{
    setTabOrder(m_lineEdit, m_fullSelectToolButton);
    setTabOrder(m_fullSelectToolButton, m_fullSelectMenuButton);
    setTabOrder(m_fullSelectMenuButton, m_minPushButton);
}

void FullMainWindow::initConnect()
{
    connect(m_lineEdit, &QLineEdit::textChanged, this, &FullMainWindow::searchAppSlot);
    connect(this, &FullMainWindow::sendSearchKeyword, m_searchAppThread, &SearchAppThread::recvSearchKeyword);
    connect(m_searchAppThread, &SearchAppThread::sendSearchResult, this, &FullMainWindow::recvSearchResult);
    connect(m_minPushButton, &QPushButton::clicked, this, &FullMainWindow::on_minPushButton_clicked);
    connect(m_fullSelectToolButton, &QToolButton::clicked, this, &FullMainWindow::on_fullSelectToolButton_clicked);
    connect(m_fullFunctionPage, &FullFunctionWidget::setFocusToSideWin, this, &FullMainWindow::setFocusToButton);
    connect(m_fullLetterPage, &FullLetterWidget::setFocusToSideWin, this, &FullMainWindow::setFocusToButton);
    connect(m_fullCommonPage, &FullCommonUseWidget::setFocusToSideWin, this, &FullMainWindow::setFocusToButton);
    connect(m_fullResultPage, &FullSearchResultWidget::setFocusToSideWin, this, &FullMainWindow::setFocusToButton);
    connect(this, &FullMainWindow::sendSetFocusToCom, m_fullCommonPage, &FullCommonUseWidget::selectFirstItemTab);
    connect(this, &FullMainWindow::sendSetFocusToLet, m_fullLetterPage, &FullLetterWidget::setFocusToThis);
    connect(this, &FullMainWindow::sendSetFocusToFun, m_fullFunctionPage, &FullFunctionWidget::setFocusToThis);
    connect(this, &FullMainWindow::sendSetFocusToResult, m_fullResultPage, &FullSearchResultWidget::selectFirstItemTab);
//    connect(m_fullSelectMenuButton, &QToolButton::clicked, this, &FullMainWindow::on_fullSelectMenuButton_clicked);
    connect(m_fullCommonPage, &FullCommonUseWidget::sendUpdateOtherView, this, &FullMainWindow::sendUpdateOtherView);
    connect(m_fullFunctionPage, &FullFunctionWidget::sendUpdateCommonView, m_fullCommonPage, &FullCommonUseWidget::updateListViewSlot);
    connect(m_fullLetterPage, &FullLetterWidget::sendUpdateCommonView, m_fullCommonPage, &FullCommonUseWidget::updateListViewSlot);
    connect(m_fullCommonPage, &FullCommonUseWidget::sendHideMainWindowSignal, this, &FullMainWindow::fullWindowHide);
    connect(m_fullFunctionPage, &FullFunctionWidget::sendHideMainWindowSignal, this, &FullMainWindow::fullWindowHide);
    connect(m_fullLetterPage, &FullLetterWidget::sendHideMainWindowSignal, this, &FullMainWindow::fullWindowHide);
    connect(m_fullResultPage, &FullSearchResultWidget::sendHideMainWindowSignal, this, &FullMainWindow::fullWindowHide);
    connect(m_dropDownMenu, &MenuBox::triggered, this, &FullMainWindow::on_fullSelectMenuButton_triggered);
    connect(m_dropDownMenu, &MenuBox::sendMainWinActiveSignal, [ = ]() {
        selectIconAnimation(false);
    });
}

void FullMainWindow::updateView()
{
    m_fullCommonPage->updateListView();
    m_fullFunctionPage->updateAppListView();
    m_fullLetterPage->updateAppListView();
}

void FullMainWindow::changeStyle()
{
    QPalette linePe;
    QString buttonColorDefault;
    QString buttonColorHover;
    QString buttonColorPress;
    QColor buttonColor;

    if (QGSettings::isSchemaInstalled(QString("org.ukui.style").toLocal8Bit())) {
        QGSettings gsetting(QString("org.ukui.style").toLocal8Bit());

        if (gsetting.keys().contains(QString("styleName"))) {
            if (/*gsetting.get("style-name").toString() == "ukui-light"*/false) {
                buttonColorDefault = "rgba(16, 23, 29, 0.06)";
                buttonColorHover = "rgba(16, 23, 29, 0.12)";
                buttonColorPress = "rgba(16, 23, 29, 0.17)";
            } else {
                buttonColor = linePe.color(QPalette::Light);
                QRgb rgbDefault = qRgba(buttonColor.red(), buttonColor.green(), buttonColor.blue(), 25);
                buttonColorDefault = "#" +  QString::number(rgbDefault, 16);
                QRgb rgbHover = qRgba(buttonColor.red(), buttonColor.green(), buttonColor.blue(), 50);
                buttonColorHover = "#" + QString::number(rgbHover, 16);
                QRgb rgbPress = qRgba(buttonColor.red(), buttonColor.green(), buttonColor.blue(), 75);
                buttonColorPress = "#" + QString::number(rgbPress, 16);
            }
        }
    }

    m_buttonStyle = QString("%1{border-radius:24px; background:" + buttonColorDefault + ";}"
                            "%1:hover {border-radius:24px; background:" + buttonColorHover + ";}"
                            "%1:pressed {border-radius:24px; background:" + buttonColorPress + ";}");
    m_fullSelectToolButton->setStyleSheet(m_buttonStyle.arg("QPushButton"));
    m_fullSelectMenuButton->setIcon(QPixmap(":/data/img/mainviewwidget/DM-arrow-2x.png"));
    m_minPushButton->setIcon(getCurIcon(":/data/img/mainviewwidget/full-min.svg", false));
    m_minPushButton->setProperty("useIconHighlightEffect", 0x0);
}

void FullMainWindow::on_minPushButton_clicked()
{
    Q_EMIT showNormalWindow();
}

void FullMainWindow::paintEvent(QPaintEvent *event)
{
    double transparency = getTransparency();
    QPainterPath path;
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);  // 反锯齿;
    painter.setPen(Qt::transparent);
    painter.setBrush(this->palette().dark());
    painter.setPen(Qt::transparent);
    painter.setOpacity(transparency);
    painter.drawRect(this->rect());
    path.addRect(this->rect());
    KWindowEffects::enableBlurBehind(this->winId(), true, QRegion(path.toFillPolygon().toPolygon()));
    QMainWindow::paintEvent(event);
}

bool FullMainWindow::eventFilter(QObject *watched, QEvent *event)
{
    if (watched == m_lineEdit) {
        m_isSearching = true;
        char style[200];

        if (event->type() == QEvent::FocusIn) {
            sprintf(style, "QLineEdit{border:2px solid %s;background-color:%s;border-radius:17px;color:#ffffff;}",
                    QueryLineEditClickedBorder, QueryLineEditClickedBackground);
            m_lineEdit->setStyleSheet(style);

            if (m_lineEdit->text().isEmpty()) {
                qDebug() << "bool FullMainWindow::eventFilter(QObject *watched, QEvent *event)" << m_queryWid->layout()->count();

                if (m_queryWid->layout()->count() == 2) {
                    m_queryWid->layout()->removeWidget(m_queryText);
                    m_queryText->setParent(nullptr);
                }

                m_queryWid->setGeometry(6, 2, m_queryIcon->width() + 5, Style::QueryLineEditHeight);
                m_queryWid->layout()->setAlignment(Qt::AlignVCenter);
                m_lineEdit->setTextMargins(26, 0, 0, 0);
            }
        } else if (event->type() == QEvent::FocusOut && m_lineEdit->text().isEmpty()) {
            sprintf(style, "QLineEdit{border:1px solid %s;background-color:%s;border-radius:17px;color:#ffffff;}",
                    QueryLineEditClickedBorderDefault, QueryLineEditClickedBackground);
            m_lineEdit->setStyleSheet(style);
            resetEditline();
        }

        if (event->type() == QEvent::KeyPress) {
            QKeyEvent *ke = (QKeyEvent *)event;

            if (ke->key() == Qt::Key_Enter || ke->key() == Qt::Key_Return || ke->key() == Qt::Key_Down) {
                Q_EMIT sendSetFocusToResult();
            }
        }
    }

    if (watched == m_minPushButton) {
        if (event->type() == QEvent::KeyPress) {
            QKeyEvent *ke = (QKeyEvent *)event;

            if (ke->key() == Qt::Key_Tab) {
                if (m_fullStackedWidget->currentIndex() == 0) {
                    Q_EMIT sendSetFocusToCom();
                } else if (m_fullStackedWidget->currentIndex() == 1) {
                    Q_EMIT sendSetFocusToLet();
                } else if (m_fullStackedWidget->currentIndex() == 2) {
                    Q_EMIT sendSetFocusToFun();
                } else {
                    Q_EMIT sendSetFocusToResult();
                }

                return true;
            }
        }
    }

    if (watched == m_minPushButton || watched == m_fullSelectToolButton || watched == m_fullSelectMenuButton) {
        if (event->type() == QEvent::KeyPress) {
            QKeyEvent *ke = (QKeyEvent *)event;

            if (ke->key() == Qt::Key_Up || ke->key() == Qt::Key_Down ||
                ke->key() == Qt::Key_Right || ke->key() == Qt::Key_Left) {
                return true;
            }
        }
    }

    if (watched == m_fullSelectMenuButton) {
         if (event->type() == QEvent::MouseButtonPress) {
             selectIconAnimation(true);
             return true;
         }
    }

    return QWidget::eventFilter(watched, event);
}

void FullMainWindow::iconAnimationFinished()
{
    m_dropDownMenu->raise();
    m_dropDownMenu->exec(m_fullSelectMenuButton->mapToGlobal(QPoint(m_fullSelectMenuButton->width()
                                                                -Style::DropMenuWidth, 45)));
}

void FullMainWindow::keyPressEvent(QKeyEvent *e)
{
    if (e->type() == QEvent::KeyPress) {
        if ((e->key() >= Qt::Key_0 && e->key() <= Qt::Key_9) || (e->key() >= Qt::Key_A && e->key() <= Qt::Key_Z)) {
            qDebug() << "void MainWindow::keyPressEvent(QKeyEvent *e)" << e->text();
            m_lineEdit->setFocus();
            m_lineEdit->setText(e->text());
        }

        if (e->key() == Qt::Key_Backspace) {
            if (!m_lineEdit->text().isEmpty()) {
                m_lineEdit->setText("");
            }
        }
    }
}

void FullMainWindow::selectIconAnimation(const bool &flag)
{
    iconAnimation = new QPropertyAnimation(m_fullSelectMenuButton, "rotation", this);

    if (flag) {
        connect(iconAnimation, &QPropertyAnimation::finished, this, &FullMainWindow::iconAnimationFinished);

        if (m_fullSelectMenuButton->property("rotation") == 0) {
            iconAnimation->setStartValue(0);
            iconAnimation->setEndValue(-180);
        }
    } else {
        if (m_fullSelectMenuButton->property("rotation") == -180) {
            iconAnimation->setStartValue(-180);
            iconAnimation->setEndValue(0);
        }
    }

    iconAnimation->setEasingCurve(QEasingCurve::Linear);
    iconAnimation->setDuration(300);
    iconAnimation->start(QPropertyAnimation::DeleteWhenStopped);
}

void FullMainWindow::fullWindowHide()
{
    this->hide();
}

void FullMainWindow::resetEditline()
{
    m_lineEdit->clear();
    m_lineEdit->clearFocus();
    m_fullSelectToolButton->setFocus();
    if (m_queryWid->layout()->children().isEmpty()) {
        m_queryWid->layout()->addWidget(m_queryIcon);
        m_queryWid->layout()->addWidget(m_queryText);
    }
    m_queryIcon->adjustSize();
    m_queryText->adjustSize();
    m_queryWid->layout()->setAlignment(Qt::AlignCenter);
    m_queryWid->setFixedSize(372, 36);
}

bool FullMainWindow::event(QEvent *event)
{
    if (event->type() == QEvent::ActivationChange) {
        qDebug() << " * 鼠标点击窗口外部事件";

        if (QApplication::activeWindow() != this) {
            this->hide();
        } /*else {

            fullLetterPage->repaintWidget();
            fullFunctionPage->repaintWidget();
        }*/
    }

    if (event->type() == QEvent::KeyPress) {
        QKeyEvent *keyEvent = (QKeyEvent *) event;

        if (keyEvent->key() == Qt::Key_Enter || keyEvent->key() == Qt::Key_Return) {
            if (m_fullSelectToolButton->hasFocus()) {
                m_fullSelectToolButton->click();
            } else if (m_fullSelectMenuButton->hasFocus()) {
                QApplication::postEvent(m_fullSelectMenuButton, new QEvent(QEvent::MouseButtonPress));
            }

            if (m_lineEdit->hasFocus()) {
                m_fullResultPage->setFocus();
            } else {
                QWidget *current_focus_widget;
                current_focus_widget = QWidget::focusWidget();
                QPushButton *le = qobject_cast<QPushButton *>(current_focus_widget);

                if (le != nullptr) {
                    le->clicked();
                }
            }
        }

        if (keyEvent->key() == Qt::Key_Escape) {
            m_lineEdit->clear();
            this->hide();
        }
    }

    return QWidget::event(event);
}

void FullMainWindow::setFocusToButton()
{
    qDebug() << "设置lineEdit焦点";
    m_lineEdit->setFocus();
}

void FullMainWindow::searchAppSlot(QString arg)
{
    if (!arg.isEmpty()) { //切换至搜索模块
        Q_EMIT sendSearchKeyword(arg);
        m_searchAppThread->start();
        m_fullStackedWidget->setCurrentIndex(3);
    } else {
        m_fullStackedWidget->setCurrentIndex(m_state);
        m_fullFunctionPage->repaintWidget();
        m_fullLetterPage->repaintWidget();
    }
}

void FullMainWindow::mousePressEvent(QMouseEvent *event)
{
    resetEditline();
    if (event->button() == Qt::LeftButton) {
        this->hide();
    }
    return QMainWindow::mousePressEvent(event);
}

void FullMainWindow::recvSearchResult(QVector<QStringList> arg)
{
    m_searchAppThread->quit();
    m_fullResultPage->updateAppListView(arg);
}

void FullMainWindow::on_fullSelectToolButton_clicked()
{
    resetEditline();
    m_fullSelectToolButton->setFocus();
    selectIconAnimation(false);

    if (m_fullStackedWidget->currentIndex() == 0) {
        on_fullSelectMenuButton_triggered(m_letterAction);
    } else if (m_fullStackedWidget->currentIndex() == 1) {
        on_fullSelectMenuButton_triggered(m_funcAction);
    } else if (m_fullStackedWidget->currentIndex() == 2) {
        on_fullSelectMenuButton_triggered(m_allAction);
    }
}

void FullMainWindow::on_fullSelectMenuButton_clicked()
{
    resetEditline();
}

void FullMainWindow::on_fullSelectMenuButton_triggered(QAction *arg1)
{
    selectIconAnimation(false);

    if (arg1 == m_allAction) {
        m_fullStackedWidget->setCurrentIndex(0);
        m_fullCommonPage->repaintWidget();
        //fullCommonPage->updateListView();
        m_state = 0;
        m_fullSelectToolButton->setIcon(QIcon(":/data/img/mainviewwidget/full-all-2x.png"));
        m_allAction->setChecked(true);
        m_letterAction->setChecked(false);
        m_funcAction->setChecked(false);
    } else if (arg1 == m_letterAction) {
        m_fullStackedWidget->setCurrentIndex(1);
        m_fullLetterPage->repaintWidget();
        m_state = 1;
        m_fullSelectToolButton->setIcon(QIcon(":/data/img/mainviewwidget/full-letter.svg"));
        m_allAction->setChecked(false);
        m_letterAction->setChecked(true);
        m_funcAction->setChecked(false);
    } else if (arg1 == m_funcAction) {
        m_fullStackedWidget->setCurrentIndex(2);
        m_fullFunctionPage->repaintWidget();
        m_state = 2;
        m_fullSelectToolButton->setIcon(QIcon(":/data/img/mainviewwidget/full-function.svg"));
        m_allAction->setChecked(false);
        m_letterAction->setChecked(false);
        m_funcAction->setChecked(true);
    }
}

void FullMainWindow::repaintWidget()
{
    m_fullCommonPage->repaintWidget();
    m_fullFunctionPage->repaintWidget();
    m_fullLetterPage->repaintWidget();
    m_fullResultPage->repaintWidget();
}

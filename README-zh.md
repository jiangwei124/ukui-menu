# ukui-menu

![build](https://github.com/ukui/ukui-menu/workflows/Check%20build/badge.svg?branch=master)


一个超好用的UKUI菜单组件，提供通用和所有软件混合排序模块，字母分类模块，功能分类模模块以及搜索模块。提供最喜爱和最近文件功能，并且你可以通过右键菜单来优化你最喜欢的区域。提供两种显示模式：默认窗口模式以及全屏窗口模式。

# 项目结构
## data/img

用于存储项目中所需要用到的icon文件。

## src/BackProcess

后台数据处理方法，包括DBUS、路径监听、数据采集和搜索功能。

## src/QtSingleApplication

单例模式处理。

## src/UserInterface

UI实现。

## src/UtilityFunction

通用函数的实现，包括数据库操作。

## translations

翻译文件。

# 如何提交bug
Bug需要提交到UKUI的bug收集系统：

    https://github.com/ukui/ukui-menu/issues

你需要创建一个github账号。

请阅读 HACKING 文件，以获得关于向何处发送此包的更改或错误修复的信息。

